package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"syscall"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"go.uber.org/zap"

	"form3/payment/internal"
	"form3/payment/internal/config"
)

func main() {
	var rootCmd = &cobra.Command{Use: "server"}
	rootCmd.AddCommand(cmdStart())

	if len(os.Args) == 1 {
		rootCmd.SetArgs([]string{"start"})
	}

	rootCmd.Execute() // nolint: errcheck
}

func cmdStart() *cobra.Command {
	return &cobra.Command{
		Use: "start",
		Run: func(cmd *cobra.Command, args []string) {
			baseLogger, err := zap.NewProduction()
			if err != nil {
				fmt.Println(errors.Wrap(err, "logger initialization error"))
				os.Exit(1)
			}
			defer baseLogger.Sync() // nolint: errcheck
			logger := baseLogger.Sugar()

			logger.Info("Starting server")
			payload, err := ioutil.ReadFile("server.toml")
			if err != nil {
				logger.Fatal(errors.Wrap(err, "load file error"))
			}

			cfg, err := config.Parse(payload)
			if err != nil {
				logger.Fatal(errors.Wrap(err, "config parse error"))
			}

			client := internal.NewClient(cfg)
			if err := client.Start(); err != nil {
				logger.Fatal(errors.Wrap(err, "client start error"))
			}

			wait()

			logger.Info("Stopping server")
			if err := client.Stop(); err != nil {
				err = errors.Wrap(err, "client stop error")
				logger.Fatal(err)
			}

			logger.Info("Server stoped")
		},
	}
}

func wait() {
	var done = make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGINT, syscall.SIGTERM)
	<-done
}
