//go:generate moq -pkg payment -out payment_mock_test.go ../../domain PaymentService

package payment

import (
	"context"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"

	"form3/payment/internal/domain"
)

func TestClientCreate(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name      string
		payment   domain.Payment
		service   domain.PaymentService
		shouldErr require.ErrorAssertionFunc
	}{
		{
			"success",
			domain.Payment{
				Attribute: domain.PaymentAttribute{Amount: "100.00"},
			},
			&PaymentServiceMock{
				CreateFunc: func(ctx context.Context, payment domain.Payment) (string, error) {
					return "", nil
				},
			},
			require.NoError,
		},
		{
			"should have a service error",
			domain.Payment{
				Attribute: domain.PaymentAttribute{Amount: "100.00"},
			},
			&PaymentServiceMock{
				CreateFunc: func(ctx context.Context, payment domain.Payment) (string, error) {
					return "", errors.New("service error")
				},
			},
			require.Error,
		},
		{
			"should not set the ID",
			domain.Payment{
				ID:        "123",
				Attribute: domain.PaymentAttribute{Amount: "100.00"},
			},
			&PaymentServiceMock{
				CreateFunc: func(ctx context.Context, payment domain.Payment) (string, error) {
					return "", nil
				},
			},
			require.Error,
		},
		{
			"should have a validation error",
			domain.Payment{},
			&PaymentServiceMock{
				CreateFunc: func(ctx context.Context, payment domain.Payment) (string, error) {
					return payment.ID, nil
				},
			},
			require.Error,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			client := NewClient(ClientConfig{Service: tt.service})
			paymentID, err := client.Create(context.Background(), tt.payment)
			tt.shouldErr(t, err)
			if err != nil {
				return
			}
			require.NotEqual(t, "", paymentID)
		})
	}
}

func TestClientUpdate(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name      string
		payment   domain.Payment
		service   domain.PaymentService
		shouldErr require.ErrorAssertionFunc
	}{
		{
			"success",
			domain.Payment{
				ID:        "123",
				Attribute: domain.PaymentAttribute{Amount: "100.00"},
			},
			&PaymentServiceMock{
				UpdateFunc: func(ctx context.Context, payment domain.Payment) error {
					return nil
				},
			},
			require.NoError,
		},
		{
			"should have a service error",
			domain.Payment{
				ID:        "123",
				Attribute: domain.PaymentAttribute{Amount: "100.00"},
			},
			&PaymentServiceMock{
				UpdateFunc: func(ctx context.Context, payment domain.Payment) error {
					return errors.New("service error")
				},
			},
			require.Error,
		},
		{
			"should have a validation error",
			domain.Payment{ID: "123"},
			&PaymentServiceMock{
				CreateFunc: func(ctx context.Context, payment domain.Payment) (string, error) {
					return payment.ID, nil
				},
			},
			require.Error,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			client := NewClient(ClientConfig{Service: tt.service})
			err := client.Update(context.Background(), tt.payment)
			tt.shouldErr(t, err)
		})
	}
}
