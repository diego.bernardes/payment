package payment

import (
	"context"

	"github.com/pkg/errors"

	"form3/payment/internal/domain"
)

// ClientConfig has all the information needed to configure a client.
type ClientConfig struct {
	Service domain.PaymentService
}

// Client implement payment business logic.
type Client struct {
	config ClientConfig
}

// List a collection of payments.
func (c Client) List(ctx context.Context, pagination domain.Pagination) ([]domain.Payment, domain.Pagination, error) {
	payments, pagination, err := c.config.Service.List(ctx, pagination)
	if len(payments) == 0 {
		payments = make([]domain.Payment, 0)
	}
	return payments, pagination, err
}

// Find a payment by id.
func (c Client) Find(ctx context.Context, id string) (domain.Payment, error) {
	return c.config.Service.Find(ctx, id)
}

// Create a payment.
func (c Client) Create(ctx context.Context, payment domain.Payment) (string, error) {
	if payment.ID != "" {
		return "", customError{err: errors.New("'Payment.ID' should not be set"), invalid: true}
	}
	payment.ID = genID()

	if err := payment.Valid(); err != nil {
		return "", customError{err: errors.New("invalid payment"), invalid: true}
	}

	if _, err := c.config.Service.Create(ctx, payment); err != nil {
		return "", errors.Wrap(err, "create error")
	}

	return payment.ID, nil
}

// Update a payment.
func (c Client) Update(ctx context.Context, payment domain.Payment) error {
	if err := payment.Valid(); err != nil {
		return customError{err: errors.New("invalid payment"), invalid: true}
	}

	if err := c.config.Service.Update(ctx, payment); err != nil {
		return errors.Wrap(err, "update error")
	}

	return nil
}

// Delete a payment.
func (c Client) Delete(ctx context.Context, id string) error {
	return c.config.Service.Delete(ctx, id)
}

// NewClient return a configured client.
func NewClient(config ClientConfig) Client {
	return Client{config: config}
}
