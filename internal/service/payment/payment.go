package payment

import (
	"math/rand"
	"time"

	"github.com/oklog/ulid"
)

type customError struct {
	err     error
	invalid bool
}

// Error return the error message.
func (ce customError) Error() string { return ce.err.Error() }

// NotFound is not used by this service.
func (ce customError) NotFound() bool { return false }

// Invalid is used to indicate if the operation or its arguments are not valid.
func (ce customError) Invalid() bool { return ce.invalid }

func genID() string {
	t := time.Now().UTC()
	entropy := rand.New(rand.NewSource(t.UnixNano()))
	return ulid.MustNew(ulid.Timestamp(t), entropy).String()
}
