package internal

import (
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"form3/payment/internal/repository/postgres"
	"form3/payment/internal/service/payment"
	"form3/payment/internal/transport/http"
)

// ClientConfig has all the logic needed to start a client.
type ClientConfig struct {
	Logger   *zap.SugaredLogger
	HTTP     http.ServerConfig
	Postgres postgres.ClientConfig
}

// Client has the logic to start the application.
type Client struct {
	config   ClientConfig
	database *database
	server   *http.Server
}

// Start the application.
func (c *Client) Start() error {
	// Start the database.
	c.database = newDatabase(c)
	if err := c.database.start(); err != nil {
		return errors.Wrap(err, "database start error")
	}

	// Create the repository.
	repository := postgres.NewPayment(c.database.postgres)

	// Start the HTTP server.
	c.config.HTTP.Logger = c.config.Logger
	c.config.HTTP.Service = payment.NewClient(payment.ClientConfig{Service: repository})
	c.server = http.NewServer(c.config.HTTP)
	c.server.Start()

	return nil
}

// Stop the application.
func (c *Client) Stop() error {
	if err := c.server.Stop(); err != nil {
		return errors.Wrap(err, "server stop error")
	}
	c.database.stop()
	return nil
}

// NewClient return a configured client.
func NewClient(config ClientConfig) Client {
	return Client{config: config}
}
