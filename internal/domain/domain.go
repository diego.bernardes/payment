package domain

import (
	"context"
	"errors"
	"time"
)

// PaymentService has payment business logic.
type PaymentService interface {
	List(ctx context.Context, pagination Pagination) ([]Payment, Pagination, error)
	Find(ctx context.Context, id string) (Payment, error)
	Create(ctx context.Context, payment Payment) (string, error)
	Update(ctx context.Context, payment Payment) error
	Delete(ctx context.Context, id string) error
}

// PaymentServiceError represents the error returned from the service.
type PaymentServiceError interface {
	Error() string
	NotFound() bool
	Invalid() bool
}

// Pagination control the number of records to be fetched.
type Pagination struct {
	Limit  int    `json:"limit"`
	Offset string `json:"offset,omitempty"`
	Total  int    `json:"total"`
}

// Payment has all the information to represent a payment.
type Payment struct {
	ID             string           `json:"id"`
	Version        int              `json:"version"`
	OrganizationID string           `json:"organizationID"`
	Attribute      PaymentAttribute `json:"attribute"`
}

// Valid check if the payment is valid, including its dependencies.
//
// The payment struct is very large and I think it's out of the scope of this test to ensure more
// complex validations. Wherever, this function shows how this could be done. The current
// validations are present to generate some error cases for the tests/API.
func (p Payment) Valid() error {
	if p.ID == "" {
		return errors.New("missing 'ID'")
	}

	if p.Attribute.Amount == "" {
		return errors.New("missing 'Attribute.Amount'")
	}

	return nil
}

// PaymentAttribute has the payment attributes.
type PaymentAttribute struct {
	Amount               string                        `json:"amount"`
	Beneficiary          PaymentAttributeFinancialUnit `json:"beneficiary"`
	Debtor               PaymentAttributeFinancialUnit `json:"debtor"`
	Charge               PaymentCharge                 `json:"charge"`
	Currency             string                        `json:"currency"`
	E2EReference         string                        `json:"e2eReference"`
	FX                   PaymentAttributeFX            `json:"fx"`
	NumericReference     string                        `json:"numericReference"`
	PaymentID            string                        `json:"paymentID"`
	PaymentPurpose       string                        `json:"paymentPurpose"`
	PaumentScheme        string                        `json:"paumentScheme"`
	PaumentType          string                        `json:"paumentType"`
	ProcessingDate       time.Time                     `json:"processingDate"`
	Reference            string                        `json:"reference"`
	SchemePaymentSubType string                        `json:"schemePaymentSubType"`
	SchemePaymentType    string                        `json:"schemePaymentType"`
	Sponsor              PaymentAttributeSponsor       `json:"sponsor"`
}

// PaymentAttributeFinancialUnit represent a financial unit.
type PaymentAttributeFinancialUnit struct {
	AccountName   string `json:"accountName"`
	AccountNumber string `json:"accountNumber"`
	AccountCode   string `json:"accountCode"`
	AccountType   int    `json:"accountType"`
	Address       string `json:"address"`
	BankID        string `json:"bankID"`
	BankIDCode    string `json:"bankIDCode"`
	Name          string `json:"name"`
}

// PaymentCharge has a more detailed information about the amount of money being trasfered.
type PaymentCharge struct {
	BearerCode             string                      `json:"bearerCode"`
	SenderCharge           []PaymentChargeSenderCharge `json:"senderCharge"`
	ReceiverChargeAmount   string                      `json:"receiverChargeAmount"`
	ReceiverChargeCurrency string                      `json:"receiverChargeCurrency"`
}

// PaymentChargeSenderCharge has the information about the amount of money being transfered.
type PaymentChargeSenderCharge struct {
	Amount   string `json:"amount"`
	Currency string `json:"currency"`
}

// PaymentAttributeFX .
type PaymentAttributeFX struct {
	ContractReference string `json:"contractReference"`
	ExchangeRate      string `json:"exchangeRate"`
	OriginalAmount    string `json:"originalAmount"`
	OriginalCurrency  string `json:"originalCurrency"`
}

// PaymentAttributeSponsor .
type PaymentAttributeSponsor struct {
	AccountNumber string `json:"accountNumber"`
	BandID        string `json:"bandID"`
	BandIDCode    string `json:"bandIDCcode"`
}
