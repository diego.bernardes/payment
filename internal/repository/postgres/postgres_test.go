// +build integration

package postgres

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"github.com/pkg/errors"
)

var client Client // nolint: gochecknoglobals

func TestMain(m *testing.M) {
	// Bootstrap the database with the needed initial structure.
	if err := bootstrap(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// Initialize the connection used by the tests.
	var err error
	client, err = connect(false)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer client.Close()

	// Execute the tests.
	os.Exit(m.Run())
}

func connect(skipInitStatement bool) (Client, error) {
	config := ClientConfig{
		User:              os.Getenv("POSTGRES_USER"),
		Password:          os.Getenv("POSTGRES_PASSWORD"),
		Database:          os.Getenv("POSTGRES_DB"),
		Host:              os.Getenv("POSTGRES_HOST"),
		skipInitStatement: skipInitStatement,
	}
	return NewClient(config)
}

func bootstrap() error {
	client, err := connect(true)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer client.Close()

	payload, err := ioutil.ReadFile("../../../misc/sql/bootstrap.sql")
	if err != nil {
		return errors.Wrap(err, "read bootstrap sql file error")
	}

	if _, err = client.connPool.Exec(string(payload)); err != nil {
		return errors.Wrap(err, "bootstrap database error")
	}

	return nil
}

func checkError(kind string) func(*testing.T, error) {
	return func(t *testing.T, err error) {
		t.Helper()

		if err == nil || kind == "" {
			return
		}

		nerr, ok := errors.Cause(err).(customError)
		require.True(t, ok, "it's expected the error to be a customError")
		switch kind {
		case "notFound":
			require.True(t, nerr.NotFound(), "expected a not found error")
		default:
			require.FailNowf(t, "undefined customError option '%s'", kind)
		}
	}
}
