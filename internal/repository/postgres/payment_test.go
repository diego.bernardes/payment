// +build integration

package postgres

import (
	"context"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"

	"form3/payment/internal/domain"
)

func TestPayment(t *testing.T) {
	t.Run("Create", testPaymentCreate)
	t.Run("Find", testPaymentFind)
	t.Run("List", testPaymentList)
	t.Run("Update", testPaymentUpdate)
	t.Run("Delete", testPaymentDelete)
}

func testPaymentCreate(t *testing.T) {
	tests := []struct {
		name      string
		ctxGen    func() (context.Context, func())
		payment   domain.Payment
		assertion require.ErrorAssertionFunc
	}{
		{
			"success #1",
			func() (context.Context, func()) { return context.Background(), func() {} },
			domain.Payment{ID: "123"},
			require.NoError,
		},
		{
			"success #2",
			func() (context.Context, func()) { return context.Background(), func() {} },
			domain.Payment{ID: "456"},
			require.NoError,
		},
		{
			"not allow to create the payment with an already existing id",
			func() (context.Context, func()) { return context.Background(), func() {} },
			domain.Payment{ID: "123"},
			require.Error,
		},
		{
			"not create the payment because the context is canceled",
			func() (context.Context, func()) { return context.WithCancel(context.Background()) },
			domain.Payment{ID: "456"},
			require.Error,
		},
	}

	payment := NewPayment(client)
	for _, tt := range tests {
		t.Run("Should "+tt.name, func(t *testing.T) {
			ctx, ctxCancel := tt.ctxGen()
			ctxCancel()

			_, err := payment.Create(ctx, tt.payment)
			tt.assertion(t, err)
			if err != nil {
				return
			}

			pp, err := payment.Find(ctx, tt.payment.ID)
			require.NoError(t, err, "should not have an error during find")
			require.Equal(t, tt.payment, pp, "should have an expected payment")
		})
	}
}

func testPaymentFind(t *testing.T) {
	tests := []struct {
		name       string
		ctxGen     func() (context.Context, func())
		payment    domain.Payment
		assertion  require.ErrorAssertionFunc
		checkError func(*testing.T, error)
	}{
		{
			"success #1",
			func() (context.Context, func()) { return context.Background(), func() {} },
			domain.Payment{ID: "123"},
			require.NoError,
			checkError(""),
		},
		{
			"success #2",
			func() (context.Context, func()) { return context.Background(), func() {} },
			domain.Payment{ID: "123"},
			require.NoError,
			checkError(""),
		},
		{
			"not found",
			func() (context.Context, func()) { return context.Background(), func() {} },
			domain.Payment{ID: "789"},
			require.Error,
			checkError("notFound"),
		},
		{
			"not find the payment because the context is canceled",
			func() (context.Context, func()) { return context.WithCancel(context.Background()) },
			domain.Payment{ID: "123"},
			require.Error,
			checkError(""),
		},
	}

	payment := NewPayment(client)
	for _, tt := range tests {
		t.Run("Should "+tt.name, func(t *testing.T) {
			ctx, ctxCancel := tt.ctxGen()
			ctxCancel()

			p, err := payment.Find(ctx, tt.payment.ID)
			tt.assertion(t, err)
			if err != nil {
				tt.checkError(t, err)
				return
			}
			require.Equal(t, p, tt.payment)
		})
	}
}

func testPaymentList(t *testing.T) {
	tests := []struct {
		name             string
		ctxGen           func() (context.Context, func())
		payments         []domain.Payment
		pagination       domain.Pagination
		paginationResult domain.Pagination
		assertion        require.ErrorAssertionFunc
	}{
		{
			"success #1",
			func() (context.Context, func()) { return context.Background(), func() {} },
			[]domain.Payment{
				{
					ID: "123",
				},
			},
			domain.Pagination{Limit: 1},
			domain.Pagination{Limit: 1, Total: 2},
			require.NoError,
		},
		{
			"success #2",
			func() (context.Context, func()) { return context.Background(), func() {} },
			[]domain.Payment{
				{
					ID: "123",
				},
				{
					ID: "456",
				},
			},
			domain.Pagination{Limit: 2},
			domain.Pagination{Limit: 2, Total: 2},
			require.NoError,
		},
		{
			"success #3",
			func() (context.Context, func()) { return context.Background(), func() {} },
			[]domain.Payment{
				{
					ID: "456",
				},
			},
			domain.Pagination{Limit: 1, Offset: "123"},
			domain.Pagination{Limit: 1, Offset: "123", Total: 2},
			require.NoError,
		},
		{
			"success #4",
			func() (context.Context, func()) { return context.Background(), func() {} },
			[]domain.Payment{},
			domain.Pagination{Limit: 1, Offset: "456"},
			domain.Pagination{Limit: 1, Offset: "456", Total: 2},
			require.NoError,
		},
		{
			"not find the payments because the context is canceled",
			func() (context.Context, func()) { return context.WithCancel(context.Background()) },
			[]domain.Payment{},
			domain.Pagination{},
			domain.Pagination{},
			require.Error,
		},
	}

	payment := NewPayment(client)
	for _, tt := range tests {
		t.Run("Should "+tt.name, func(t *testing.T) {
			ctx, ctxCancel := tt.ctxGen()
			ctxCancel()

			payments, pagination, err := payment.List(ctx, tt.pagination)
			tt.assertion(t, err)
			if err != nil {
				return
			}
			require.Equal(t, tt.payments, payments)
			require.Equal(t, pagination, tt.paginationResult)
		})
	}
}

func testPaymentDelete(t *testing.T) {
	tests := []struct {
		name        string
		ctxGen      func() (context.Context, func())
		id          string
		assertion   require.ErrorAssertionFunc
		shouldExist bool
	}{
		{
			"not delete the payment because the context is canceled",
			func() (context.Context, func()) { return context.WithCancel(context.Background()) },
			"123",
			require.Error,
			true,
		},
		{
			"success #1",
			func() (context.Context, func()) { return context.Background(), func() {} },
			"123",
			require.NoError,
			false,
		},
		{
			"success #2",
			func() (context.Context, func()) { return context.Background(), func() {} },
			"456",
			require.NoError,
			false,
		},
		{
			"have an error deleting a payment that does not exist",
			func() (context.Context, func()) { return context.Background(), func() {} },
			"123",
			require.Error,
			false,
		},
	}
	payment := NewPayment(client)
	deleted := func(t *testing.T, id string, shouldExist bool) {
		t.Helper()

		_, err := payment.Find(context.Background(), id)
		if shouldExist {
			require.NoError(t, err, "should not have an error during find")
		} else {
			require.Error(t, err, "should have an error")
			checkError("notFound")(t, err)
		}
	}

	for _, tt := range tests {
		t.Run("Should "+tt.name, func(t *testing.T) {
			defer deleted(t, tt.id, tt.shouldExist)

			ctx, ctxCancel := tt.ctxGen()
			ctxCancel()

			err := payment.Delete(ctx, tt.id)
			tt.assertion(t, err)
			if err != nil {
				nerr, ok := errors.Cause(err).(customError)
				if ok && nerr.NotFound() {
					return
				}
			}
			if ctx.Err() != nil {
				return
			}
		})
	}
}

func testPaymentUpdate(t *testing.T) {
	tests := []struct {
		name            string
		ctxGen          func() (context.Context, func())
		payment         domain.Payment
		assertion       require.ErrorAssertionFunc
		shouldBeUpdated bool
	}{
		{
			"not update the payment because the context is canceled",
			func() (context.Context, func()) { return context.WithCancel(context.Background()) },
			domain.Payment{ID: "456", OrganizationID: "789"},
			require.Error,
			false,
		},
		{
			"success #1",
			func() (context.Context, func()) { return context.Background(), func() {} },
			domain.Payment{ID: "123", OrganizationID: "123"},
			require.NoError,
			true,
		},
		{
			"success #2",
			func() (context.Context, func()) { return context.Background(), func() {} },
			domain.Payment{ID: "456", OrganizationID: "456"},
			require.NoError,
			true,
		},
		{
			"have an error updating a payment that does not exist",
			func() (context.Context, func()) { return context.Background(), func() {} },
			domain.Payment{ID: "789", OrganizationID: "789"},
			require.Error,
			false,
		},
	}
	payment := NewPayment(client)
	updated := func(t *testing.T, source domain.Payment, shouldBeUpdated bool) {
		actual, err := payment.Find(context.Background(), source.ID)
		if err != nil && !errors.Cause(err).(customError).NotFound() {
			require.NoError(t, err, "should not have a error finding the payment")
		}

		if shouldBeUpdated {
			require.Equal(t, source, actual, "payment should be updated")
		} else {
			require.NotEqual(t, source, actual, "payment should not be updated")
		}
	}

	for _, tt := range tests {
		t.Run("Should "+tt.name, func(t *testing.T) {
			defer updated(t, tt.payment, tt.shouldBeUpdated)

			ctx, ctxCancel := tt.ctxGen()
			ctxCancel()

			err := payment.Update(ctx, tt.payment)
			tt.assertion(t, err)
			if err != nil {
				return
			}
		})
	}
}
