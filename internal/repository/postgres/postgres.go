package postgres

type customError struct {
	err      error
	notFound bool
}

// Error return the error message.
func (ce customError) Error() string { return ce.err.Error() }

// NotFound is used to indicate a not found error.
func (ce customError) NotFound() bool { return ce.notFound }

// Invalid is not used by this repository.
func (ce customError) Invalid() bool { return false }
