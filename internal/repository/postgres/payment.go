package postgres

import (
	"context"
	"encoding/json"

	"github.com/jackc/pgx"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"

	"form3/payment/internal/domain"
)

const (
	paymentListPreparedStatement         = "paymentList"
	paymentListNoOffsetPreparedStatement = "paymentNotOffsetList"
	paymentListCountPreparedStatement    = "paymentListCount"
	paymentFindPreparedStatement         = "paymentFind"
	paymentCreatePreparedStatement       = "paymentCreate"
	paymentDeletePreparedStatement       = "paymentDelete"
	paymentUpdatePreparedStatement       = "paymentUpdate"
)

// Payment implements the persistence logic of a payment.
type Payment struct {
	client Client
}

// List a collection of payments.
func (p Payment) List(ctx context.Context, pagination domain.Pagination) ([]domain.Payment, domain.Pagination, error) {
	g, gctx := errgroup.WithContext(ctx)

	var payments []domain.Payment
	g.Go(func() error {
		var err error
		payments, err = p.listEntries(ctx, pagination)
		return err
	})

	var rpagination domain.Pagination
	g.Go(func() error {
		var err error
		rpagination, err = p.listCount(gctx, pagination)
		return err
	})

	if err := g.Wait(); err != nil {
		return nil, domain.Pagination{}, errors.Wrap(customError{err: err}, "find error")
	}
	return payments, rpagination, nil
}

func (p Payment) listCount(ctx context.Context, pagination domain.Pagination) (domain.Pagination, error) {
	row := p.client.connPool.QueryRowEx(ctx, paymentListCountPreparedStatement, nil)
	if err := row.Scan(&pagination.Total); err != nil {
		return domain.Pagination{}, errors.Wrap(customError{err: err}, "count payments error")
	}
	return pagination, nil
}

func (p Payment) listEntries(ctx context.Context, pagination domain.Pagination) ([]domain.Payment, error) {
	var (
		rows *pgx.Rows
		err  error
	)
	if pagination.Offset == "" {
		rows, err = p.client.connPool.QueryEx(
			ctx, paymentListNoOffsetPreparedStatement, nil, pagination.Limit,
		)
	} else {
		rows, err = p.client.connPool.QueryEx(
			ctx, paymentListPreparedStatement, nil, pagination.Offset, pagination.Limit,
		)
	}
	if err != nil {
		return nil, errors.Wrap(customError{err: err}, "query error")
	}
	defer rows.Close()

	payments := make([]domain.Payment, 0)
	for rows.Next() {
		var payload string
		if err := rows.Scan(&payload); err != nil {
			return nil, errors.Wrap(customError{err: err}, "list query result parse error")
		}

		var payment domain.Payment
		if err := json.Unmarshal([]byte(payload), &payment); err != nil {
			return nil, errors.Wrap(customError{err: err}, "unmarshal payment error")
		}
		payments = append(payments, payment)
	}

	if err := rows.Err(); err != nil {
		return nil, errors.Wrap(customError{err: err}, "query result parse error")
	}
	return payments, nil
}

// Find return a single payment.
func (p Payment) Find(ctx context.Context, id string) (domain.Payment, error) {
	row := p.client.connPool.QueryRowEx(ctx, paymentFindPreparedStatement, nil, id)
	var raw []byte
	if err := row.Scan(&raw); err != nil {
		if err == pgx.ErrNoRows {
			return domain.Payment{}, errors.Wrap(customError{err: err, notFound: true}, "not found")
		}
		return domain.Payment{}, errors.Wrap(customError{err: err}, "fetch result error")
	}

	var pp domain.Payment
	if err := json.Unmarshal(raw, &pp); err != nil {
		return pp, errors.Wrap(customError{err: err}, "unmarshal payload error")
	}
	return pp, nil
}

// Create a payment.
func (p Payment) Create(ctx context.Context, payment domain.Payment) (string, error) {
	payload, err := json.Marshal(payment)
	if err != nil {
		return "", errors.Wrap(customError{err: err}, "serialization error")
	}

	cmdTag, err := p.client.connPool.ExecEx(ctx, paymentCreatePreparedStatement, nil, payment.ID, payload)
	if err != nil {
		return "", errors.Wrap(customError{err: err}, "create error")
	}
	if cmdTag.RowsAffected() == 0 {
		err := errors.New("nothing was created")
		return "", errors.Wrap(err, "expected to payment to be persisted, but nothing was done")
	}

	return payment.ID, nil
}

// Update a payment.
func (p Payment) Update(ctx context.Context, payment domain.Payment) error {
	payload, err := json.Marshal(payment)
	if err != nil {
		return errors.Wrap(customError{err: err}, "serialization error")
	}

	cmdTag, err := p.client.connPool.ExecEx(ctx, paymentUpdatePreparedStatement, nil, payload, payment.ID)
	if err != nil {
		return errors.Wrap(customError{err: err}, "update error")
	}
	if cmdTag.RowsAffected() == 0 {
		err := customError{err: errors.New("nothing was done during update"), notFound: true}
		return errors.Wrap(err, "expected to update a payment, but nothing was done")
	}

	return nil

}

// Delete a payment.
func (p Payment) Delete(ctx context.Context, id string) error {
	cmdTag, err := p.client.connPool.ExecEx(ctx, paymentDeletePreparedStatement, nil, id)
	if err != nil {
		return errors.Wrap(customError{err: err}, "delete error")
	}
	if cmdTag.RowsAffected() == 0 {
		err := customError{err: errors.New("nothing was done during delete"), notFound: true}
		return errors.Wrap(err, "expected to delete a payment, but nothing was done")
	}

	return nil
}

// NewPayment return a configured payment.
func NewPayment(client Client) Payment {
	return Payment{client: client}
}

func initPaymentPreparedStatement(conn *pgx.Conn) error {
	entries := [][]string{
		{
			paymentListPreparedStatement,
			"SELECT raw FROM payments WHERE id > (select id from payments where base_id = $1) ORDER BY id ASC LIMIT $2",
		},
		{paymentListNoOffsetPreparedStatement, "SELECT raw FROM payments ORDER BY id ASC LIMIT $1"},
		{paymentListCountPreparedStatement, "SELECT count(id) FROM payments"},
		{paymentFindPreparedStatement, "SELECT raw FROM payments WHERE base_id = $1"},
		{paymentCreatePreparedStatement, "INSERT INTO payments (base_id, raw) VALUES ($1, $2)"},
		{paymentDeletePreparedStatement, "DELETE FROM payments WHERE base_id = $1"},
		{paymentUpdatePreparedStatement, "UPDATE payments SET raw = $1 WHERE base_id = $2"},
	}

	for _, entry := range entries {
		_, err := conn.Prepare(entry[0], entry[1])
		if err != nil {
			return err
		}
	}

	return nil
}
