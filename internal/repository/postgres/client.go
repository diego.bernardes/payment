// Package postgres implement the persistence layer used by other services.
// Due to the brevity of this implementation, the information is not denormalized into multiple
// tables as it would be in a normal application. The value is stored raw at the table and only the
// searchable fields are denormalized.
package postgres

import (
	"time"

	"github.com/jackc/pgx"
	"github.com/pkg/errors"
)

// ClientConfig has all the information needed to configure a client.
type ClientConfig struct {
	User     string
	Password string
	Host     string
	Port     uint16
	Database string

	skipInitStatement bool
}

// Client holds the database connection.
type Client struct {
	connPool *pgx.ConnPool
}

// Close the connection with the database.
func (c *Client) Close() {
	c.connPool.Close()
}

func (c *Client) init(config ClientConfig) error {
	cfg := pgx.ConnPoolConfig{
		AcquireTimeout: time.Second,
		ConnConfig: pgx.ConnConfig{
			User:     config.User,
			Password: config.Password,
			Host:     config.Host,
			Port:     config.Port,
			Database: config.Database,
		},
	}
	if !config.skipInitStatement {
		cfg.AfterConnect = initPreparedStatement
	}

	var err error
	c.connPool, err = pgx.NewConnPool(cfg)
	if err != nil {
		return errors.Wrap(err, "postgres connection error")
	}
	return nil
}

// NewClient return a configured client to access the database.
func NewClient(config ClientConfig) (Client, error) {
	var c Client
	if err := c.init(config); err != nil {
		return c, errors.Wrap(err, "initialization error")
	}
	return c, nil
}

func initPreparedStatement(conn *pgx.Conn) error {
	if err := initPaymentPreparedStatement(conn); err != nil {
		return errors.Wrap(err, "payment prepared statement error")
	}
	return nil
}
