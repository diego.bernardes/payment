package internal

import (
	"github.com/pkg/errors"

	"form3/payment/internal/repository/postgres"
)

type database struct {
	client   *Client
	postgres postgres.Client
}

func (d *database) start() error {
	var err error
	d.postgres, err = postgres.NewClient(d.client.config.Postgres)
	if err != nil {
		return errors.Wrap(err, "postgres start error")
	}
	return nil
}

func (d *database) stop() {
	d.postgres.Close()
}

func newDatabase(client *Client) *database {
	return &database{client: client}
}
