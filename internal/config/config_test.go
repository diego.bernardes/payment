package config

import (
	"testing"

	"github.com/stretchr/testify/require"

	"form3/payment/internal"
	"form3/payment/internal/repository/postgres"
	"form3/payment/internal/transport/http"
)

func TestParse(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name      string
		payload   []byte
		expected  internal.ClientConfig
		assertion require.ErrorAssertionFunc
	}{
		{
			"success #1",
			[]byte(""),
			internal.ClientConfig{},
			require.NoError,
		},
		{
			"success #2",
			[]byte(`
				[transport.http]
				port      = "8080"
				base-path = "https://api.test.form3.tech/v1"

				[repository.postgres]
				user     = "postgres"
				password = "postgres"
				host     = "localhost"
				port     = 5432
				database = "form3"
			`),
			internal.ClientConfig{
				HTTP: http.ServerConfig{
					Port:     8080,
					BasePath: "https://api.test.form3.tech/v1",
				},
				Postgres: postgres.ClientConfig{
					User:     "postgres",
					Password: "postgres",
					Host:     "localhost",
					Port:     5432,
					Database: "form3",
				},
			},
			require.NoError,
		},
		{
			"fail",
			[]byte("[transport"),
			internal.ClientConfig{},
			require.Error,
		},
	}

	for _, tt := range tests {
		t.Run("Should "+tt.name, func(t *testing.T) {
			actual, err := Parse(tt.payload)
			tt.assertion(t, err)
			if err != nil {
				return
			}
			require.Equal(t, tt.expected, actual)
		})
	}
}
