package config

import (
	"bytes"

	"github.com/pkg/errors"
	"github.com/spf13/viper"

	"form3/payment/internal"
	"form3/payment/internal/repository/postgres"
	"form3/payment/internal/transport/http"
)

// Parse config payload.
func Parse(payload []byte) (internal.ClientConfig, error) {
	v, err := initViper(payload)
	if err != nil {
		return internal.ClientConfig{}, errors.Wrap(err, "viper initialization error")
	}

	return internal.ClientConfig{
		HTTP: http.ServerConfig{
			Port:     v.GetInt("transport.http.port"),
			BasePath: v.GetString("transport.http.base-path"),
		},
		Postgres: postgres.ClientConfig{
			User:     v.GetString("repository.postgres.user"),
			Password: v.GetString("repository.postgres.password"),
			Host:     v.GetString("repository.postgres.host"),
			Port:     uint16(v.GetInt("repository.postgres.port")),
			Database: v.GetString("repository.postgres.database"),
		},
	}, nil
}

func initViper(payload []byte) (*viper.Viper, error) {
	v := viper.New()
	v.SetConfigType("toml")
	if err := v.ReadConfig(bytes.NewBuffer(payload)); err != nil {
		return nil, errors.Wrap(err, "read error")
	}
	return v, nil
}
