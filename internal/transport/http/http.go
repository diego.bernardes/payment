package http

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/pkg/errors"
	"go.uber.org/zap"
)

type writer struct {
	logger *zap.SugaredLogger
}

func (wrt writer) response(w http.ResponseWriter, r interface{}, status int, headers http.Header) {
	if headers != nil {
		for key, values := range headers {
			for _, value := range values {
				w.Header().Add(key, value)
			}
		}
	}

	if r == nil {
		w.WriteHeader(status)
		return
	}

	content, err := json.Marshal(r)
	if err != nil {
		wrt.logger.Error(errors.Wrap(err, "error during response marshal"))
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	writed, err := w.Write(content)
	if err != nil {
		wrt.logger.Error(errors.Wrap(err, "error during write payload"))
	}
	if writed != len(content) {
		msg := fmt.Sprintf("invalid quantity of writed bytes, expected %d and got %d", len(content), writed)
		wrt.logger.Error(errors.Wrap(err, msg))
	}
}

func (wrt writer) error(w http.ResponseWriter, title string, err error, status int) {
	resp := struct {
		Error struct {
			Title  string            `json:"title"`
			Detail string            `json:"detail,omitempty"`
			Source map[string]string `json:"source,omitempty"`
		} `json:"error"`
	}{}

	if err != nil {
		resp.Error.Detail = err.Error()
	}

	if title != "" {
		resp.Error.Title = title
	}

	wrt.response(w, &resp, status, nil)
}

func newWriter(logger *zap.SugaredLogger) writer {
	return writer{logger: logger}
}
