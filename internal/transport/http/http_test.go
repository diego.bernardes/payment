package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"runtime"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"
)

func runner(
	t *testing.T,
	status int,
	header http.Header,
	handler func(w http.ResponseWriter, r *http.Request),
	req *http.Request,
	expectedBody []byte,
) {
	t.Helper()

	w := httptest.NewRecorder()
	handler(w, req)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	require.NoError(t, err)
	require.Equal(t, status, resp.StatusCode)
	require.Equal(t, resp.Header, header)

	if len(body) == 0 && expectedBody == nil {
		return
	}

	if header.Get("Content-Type") != "application/json" {
		require.Equal(t, expectedBody, body)
		return
	}

	b1, b2 := make(map[string]interface{}), make(map[string]interface{})
	err = json.Unmarshal(body, &b1)
	require.NoError(t, err)

	err = json.Unmarshal(expectedBody, &b2)
	require.NoError(t, err)

	require.Equal(t, b2, b1)
}

func load(t *testing.T, name string) []byte {
	t.Helper()

	_, file, _, ok := runtime.Caller(1)
	if !ok {
		panic("could not get the caller that invoked load")
	}

	path := fmt.Sprintf("%s/testdata/%s", filepath.Dir(file), name)
	f, err := os.Open(path)
	if err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("open file error '%s'", path)))
	}

	content, err := ioutil.ReadAll(f)
	if err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("read file error %s'", path)))
	}
	return content
}

type customError struct {
	err      error
	notFound bool
	invalid  bool
}

// Error return the error message.
func (ce customError) Error() string { return ce.err.Error() }

// NotFound is used to indicate a not found error.
func (ce customError) NotFound() bool { return ce.notFound }

// Invalid is not used by this repository.
func (ce customError) Invalid() bool { return ce.invalid }
