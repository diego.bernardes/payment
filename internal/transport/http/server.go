package http

import (
	"fmt"
	"net/http"
	"os"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"form3/payment/internal/domain"
)

// ServerConfig has all the configuration needed to start a server.
type ServerConfig struct {
	Port     int
	BasePath string
	Service  domain.PaymentService
	Logger   *zap.SugaredLogger
}

// Server has the HTTP logic to access the application business logic.
type Server struct {
	config ServerConfig
	base   *http.Server
}

// Start the HTTP transport.
func (s *Server) Start() {
	w := newWriter(s.config.Logger)
	h := newPayment(s.config.Service, s.config.BasePath, w)

	r := chi.NewRouter()
	r.Get("/payments", h.list)
	r.Post("/payments", h.create)
	r.Get("/payments/{id}", h.find)
	r.Put("/payments/{id}", h.update)
	r.Delete("/payments/{id}", h.delete)

	s.base = &http.Server{Addr: fmt.Sprintf(":%d", s.config.Port), Handler: r}
	go func() {
		if err := s.base.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			s.config.Logger.Error(errors.Wrap(err, "server start error"))
			os.Exit(1)
		}
	}()
}

// Stop the HTTP transport.
func (s *Server) Stop() error {
	err := s.base.Close()
	if err != nil {
		return errors.Wrap(err, "server close error")
	}
	return nil
}

// NewServer return a configured server.
func NewServer(config ServerConfig) *Server {
	return &Server{config: config}
}
