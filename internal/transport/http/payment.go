package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"

	"form3/payment/internal/domain"
)

type payment struct {
	basePath string
	service  domain.PaymentService
	writer   writer
}

func (p payment) list(w http.ResponseWriter, r *http.Request) {
	pagination, err := p.parsePagination(r.URL.Query())
	if err != nil {
		p.writer.error(w, "invalid request", err, http.StatusBadRequest)
		return
	}

	payments, resultPagination, err := p.service.List(r.Context(), pagination)
	if err != nil {
		p.handleError(err, w)
		return
	}

	result := struct {
		Pagination domain.Pagination `json:"pagination"`
		Payments   []domain.Payment  `json:"payments"`
	}{
		Pagination: resultPagination,
		Payments:   payments,
	}
	p.writer.response(w, result, http.StatusOK, nil)
}

func (p payment) find(w http.ResponseWriter, r *http.Request) {
	payment, err := p.service.Find(r.Context(), chi.URLParam(r, "id"))
	if err != nil {
		p.handleError(err, w)
		return
	}

	p.writer.response(w, payment, http.StatusOK, nil)
}

func (p payment) create(w http.ResponseWriter, r *http.Request) {
	var payment domain.Payment
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&payment); err != nil {
		p.writer.error(w, "invalid body", err, http.StatusBadRequest)
		return
	}

	id, err := p.service.Create(r.Context(), payment)
	if err != nil {
		p.handleError(err, w)
		return
	}

	h := make(http.Header)
	h.Set("Location", fmt.Sprintf("%s/payments/%s", p.basePath, id))
	p.writer.response(w, nil, http.StatusNoContent, h)
}

func (p payment) update(w http.ResponseWriter, r *http.Request) {
	var payment domain.Payment
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&payment)
	if err != nil {
		p.writer.error(w, "invalid body", err, http.StatusBadRequest)
		return
	}

	switch {
	case payment.ID == "":
		err = errors.New("'Payment.ID' can't be nil")
	case payment.ID != chi.URLParam(r, "id"):
		err = errors.New("'Payment.ID' can't be modified")
	}
	if err != nil {
		p.writer.error(w, "invalid request", err, http.StatusBadRequest)
		return
	}

	err = p.service.Update(r.Context(), payment)
	if err != nil {
		p.handleError(err, w)
		return
	}

	p.writer.response(w, payment, http.StatusOK, nil)
}

func (p payment) delete(w http.ResponseWriter, r *http.Request) {
	err := p.service.Delete(r.Context(), chi.URLParam(r, "id"))
	if err != nil {
		p.handleError(err, w)
		return
	}

	p.writer.response(w, nil, http.StatusNoContent, nil)
}

func (p payment) parsePagination(qs url.Values) (domain.Pagination, error) {
	pagination := domain.Pagination{
		Offset: qs.Get("offset"),
	}

	if rawLimit := qs.Get("limit"); rawLimit != "" {
		limit, err := strconv.ParseInt(rawLimit, 10, 64)
		if err != nil {
			return pagination, errors.Wrapf(err, "invalid limit value '%s'", rawLimit)
		}
		if limit < 0 {
			return pagination, errors.New("limit cant be less then zero")
		}
		pagination.Limit = int(limit)
	} else {
		pagination.Limit = 30
	}
	return pagination, nil
}

func (p payment) handleError(err error, w http.ResponseWriter) {
	nerr := errors.Cause(err).(domain.PaymentServiceError)
	if nerr.Invalid() {
		p.writer.error(w, "invalid request", err, http.StatusBadRequest)
	} else if nerr.NotFound() {
		p.writer.error(w, "payment not found", err, http.StatusNotFound)
	} else {
		p.writer.error(w, "internal error", err, http.StatusInternalServerError)
	}
}

func newPayment(service domain.PaymentService, basePath string, w writer) payment {
	return payment{service: service, basePath: basePath, writer: w}
}
