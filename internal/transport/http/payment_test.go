//go:generate moq -pkg http -out http_mock_test.go ../../domain PaymentService
package http

import (
	"bytes"
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"form3/payment/internal/domain"
)

func TestPaymentList(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name    string
		req     *http.Request
		status  int
		header  http.Header
		body    []byte
		service domain.PaymentService
	}{
		{
			"return a pagination error because of a invalid limit #1",
			httptest.NewRequest(http.MethodGet, "http://payments?limit=sample", nil),
			http.StatusBadRequest,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentIndex.invalidLimit.1.json"),
			&PaymentServiceMock{},
		},
		{
			"return a pagination error because of a invalid limit #2",
			httptest.NewRequest(http.MethodGet, "http://payments?limit=-1", nil),
			http.StatusBadRequest,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentIndex.invalidLimit.2.json"),
			&PaymentServiceMock{},
		},
		{
			"return an invalid request service error",
			httptest.NewRequest(http.MethodGet, "http://payments", nil),
			http.StatusBadRequest,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentIndex.invalidRequestServiceError.json"),
			&PaymentServiceMock{
				ListFunc: func(ctx context.Context, pagination domain.Pagination) ([]domain.Payment, domain.Pagination, error) {
					return nil, domain.Pagination{}, customError{err: errors.New("service error"), invalid: true}
				},
			},
		},
		{
			"return an generic service error",
			httptest.NewRequest(http.MethodGet, "http://payments", nil),
			http.StatusInternalServerError,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentIndex.serviceError.json"),
			&PaymentServiceMock{
				ListFunc: func(ctx context.Context, pagination domain.Pagination) ([]domain.Payment, domain.Pagination, error) {
					return nil, domain.Pagination{}, customError{err: errors.New("service error")}
				},
			},
		},
		{
			"return an list of payments",
			httptest.NewRequest(http.MethodGet, "http://payments", nil),
			http.StatusOK,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentIndex.success.json"),
			&PaymentServiceMock{
				ListFunc: func(ctx context.Context, pagination domain.Pagination) ([]domain.Payment, domain.Pagination, error) {
					pagination.Total = 1
					return []domain.Payment{{ID: "123"}}, pagination, nil
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := newWriter(zap.NewNop().Sugar())
			payment := newPayment(tt.service, "", w)
			runner(t, tt.status, tt.header, payment.list, tt.req, tt.body)
		})
	}
}

func TestPaymentFind(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name    string
		req     *http.Request
		status  int
		header  http.Header
		body    []byte
		service domain.PaymentService
	}{
		{
			"return an generic service error",
			httptest.NewRequest(http.MethodGet, "http://payments/123", nil),
			http.StatusInternalServerError,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentFind.serviceError.json"),
			&PaymentServiceMock{
				FindFunc: func(ctx context.Context, id string) (domain.Payment, error) {
					return domain.Payment{}, customError{err: errors.New("service error")}
				},
			},
		},
		{
			"return an not found service error",
			httptest.NewRequest(http.MethodGet, "http://payments/123", nil),
			http.StatusNotFound,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentFind.serviceNotFoundError.json"),
			&PaymentServiceMock{
				FindFunc: func(ctx context.Context, id string) (domain.Payment, error) {
					return domain.Payment{}, customError{err: errors.New("service error"), notFound: true}
				},
			},
		},
		{
			"return a payment",
			httptest.NewRequest(http.MethodGet, "http://payments/123", nil),
			http.StatusOK,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentFind.success.json"),
			&PaymentServiceMock{
				FindFunc: func(ctx context.Context, id string) (domain.Payment, error) {
					return domain.Payment{
						ID: "123",
					}, nil
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("id", "123")
			req := tt.req.WithContext(context.WithValue(tt.req.Context(), chi.RouteCtxKey, rctx))

			w := newWriter(zap.NewNop().Sugar())
			payment := newPayment(tt.service, "", w)
			runner(t, tt.status, tt.header, payment.find, req, tt.body)
		})
	}
}

func TestPaymentCreate(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name    string
		req     *http.Request
		status  int
		header  http.Header
		body    []byte
		service domain.PaymentService
	}{
		{
			"return an invalid request error",
			httptest.NewRequest(
				http.MethodPost,
				"http://payments",
				bytes.NewBuffer(load(t, "paymentCreate.input.1.json")),
			),
			http.StatusBadRequest,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentCreate.error.json"),
			&PaymentServiceMock{},
		},
		{
			"return an generic service error",
			httptest.NewRequest(
				http.MethodPost,
				"http://payments",
				bytes.NewBuffer(load(t, "paymentCreate.input.2.json")),
			),
			http.StatusInternalServerError,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentCreate.serviceError.json"),
			&PaymentServiceMock{
				CreateFunc: func(ctx context.Context, payment domain.Payment) (string, error) {
					return "", customError{err: errors.New("service error")}
				},
			},
		},
		{
			"return an invalid service error",
			httptest.NewRequest(
				http.MethodPost,
				"http://payments",
				bytes.NewBuffer(load(t, "paymentCreate.input.2.json")),
			),
			http.StatusBadRequest,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentFind.invalidServiceError.json"),
			&PaymentServiceMock{
				CreateFunc: func(ctx context.Context, payment domain.Payment) (string, error) {
					return "", customError{err: errors.New("service error"), invalid: true}
				},
			},
		},
		{
			"create a payment",
			httptest.NewRequest(
				http.MethodPost,
				"http://payments",
				bytes.NewBuffer(load(t, "paymentCreate.input.2.json")),
			),
			http.StatusNoContent,
			http.Header{"Location": []string{"https://api.test.form3.tech/v1/payments/123"}},
			nil,
			&PaymentServiceMock{
				CreateFunc: func(ctx context.Context, payment domain.Payment) (string, error) {
					return "123", nil
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := newWriter(zap.NewNop().Sugar())
			payment := newPayment(tt.service, "https://api.test.form3.tech/v1", w)
			runner(t, tt.status, tt.header, payment.create, tt.req, tt.body)
		})
	}
}

func TestPaymentUpdate(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name    string
		req     *http.Request
		status  int
		header  http.Header
		body    []byte
		service domain.PaymentService
	}{
		{
			"return an invalid request error",
			httptest.NewRequest(
				http.MethodPut,
				"http://payments/123",
				bytes.NewBuffer(load(t, "paymentUpdate.input.1.json")),
			),
			http.StatusBadRequest,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentUpdate.error.json"),
			&PaymentServiceMock{},
		},
		{
			"return an error because the id is not defined",
			httptest.NewRequest(
				http.MethodPut,
				"http://payments/123",
				bytes.NewBuffer(load(t, "paymentUpdate.input.3.json")),
			),
			http.StatusBadRequest,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentUpdate.idNotDefined.json"),
			&PaymentServiceMock{},
		},
		{
			"return an error because the id changed",
			httptest.NewRequest(
				http.MethodPut,
				"http://payments/123",
				bytes.NewBuffer(load(t, "paymentUpdate.input.4.json")),
			),
			http.StatusBadRequest,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentUpdate.idChanged.json"),
			&PaymentServiceMock{},
		},
		{
			"return a generic service error",
			httptest.NewRequest(
				http.MethodPut,
				"http://payments/123",
				bytes.NewBuffer(load(t, "paymentUpdate.input.2.json")),
			),
			http.StatusInternalServerError,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentUpdate.serviceError.json"),
			&PaymentServiceMock{
				UpdateFunc: func(ctx context.Context, payment domain.Payment) error {
					return customError{err: errors.New("service error")}
				},
			},
		},
		{
			"return a invalid service error",
			httptest.NewRequest(
				http.MethodPut,
				"http://payments/123",
				bytes.NewBuffer(load(t, "paymentUpdate.input.2.json")),
			),
			http.StatusBadRequest,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentUpdate.invalidServiceError.json"),
			&PaymentServiceMock{
				UpdateFunc: func(ctx context.Context, payment domain.Payment) error {
					return customError{err: errors.New("service error"), invalid: true}
				},
			},
		},
		{
			"update a payment",
			httptest.NewRequest(
				http.MethodPut,
				"http://payments/123",
				bytes.NewBuffer(load(t, "paymentUpdate.input.2.json")),
			),
			http.StatusOK,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentUpdate.success.json"),
			&PaymentServiceMock{
				UpdateFunc: func(ctx context.Context, payment domain.Payment) error { return nil },
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("id", "123")
			req := tt.req.WithContext(context.WithValue(tt.req.Context(), chi.RouteCtxKey, rctx))

			w := newWriter(zap.NewNop().Sugar())
			payment := newPayment(tt.service, "", w)
			runner(t, tt.status, tt.header, payment.update, req, tt.body)
		})
	}
}

func TestPaymentDelete(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name    string
		req     *http.Request
		status  int
		header  http.Header
		body    []byte
		service domain.PaymentService
	}{
		{
			"return a service not found error",
			httptest.NewRequest(http.MethodDelete, "http://payments/123", nil),
			http.StatusNotFound,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentDelete.serviceNotFoundError.json"),
			&PaymentServiceMock{
				DeleteFunc: func(ctx context.Context, id string) error {
					return customError{err: errors.New("service error"), notFound: true}
				},
			},
		},
		{
			"return a generic service error",
			httptest.NewRequest(http.MethodDelete, "http://payments/123", nil),
			http.StatusInternalServerError,
			http.Header{"Content-Type": []string{"application/json"}},
			load(t, "paymentDelete.serviceError.json"),
			&PaymentServiceMock{
				DeleteFunc: func(ctx context.Context, id string) error {
					return customError{err: errors.New("service error")}
				},
			},
		},
		{
			"delete the payment",
			httptest.NewRequest(http.MethodDelete, "http://payments/123", nil),
			http.StatusNoContent,
			http.Header{},
			nil,
			&PaymentServiceMock{
				DeleteFunc: func(ctx context.Context, id string) error { return nil },
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("id", "123")
			req := tt.req.WithContext(context.WithValue(tt.req.Context(), chi.RouteCtxKey, rctx))

			w := newWriter(zap.NewNop().Sugar())
			payment := newPayment(tt.service, "", w)
			runner(t, tt.status, tt.header, payment.delete, req, tt.body)
		})
	}
}
