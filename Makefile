DOCKER_IMAGE ?= golang:1.12.4-stretch
GO111MODULES  = on

configure:
	@git config pull.rebase true
	@git config remote.origin.prune true
	@git config branch.master.mergeoptions "--ff-only"

pre-pr: test linter linter-vendor

test:
ifeq ($(EXEC_CONTAINER), false)
ifeq ($(TAGS),)
	@go test -mod vendor -failfast -race -covermode=atomic -coverprofile=test.cover ./...
else
	@go test -mod vendor -failfast -race -coverprofile=test.cover -tags $(TAGS) ./...
endif
	@go tool cover -func=test.cover
	@rm -f test.cover
else
	@TARGET=test TAGS=$(TAGS) $(MAKE) docker-exec
endif

linter:
ifeq ($(EXEC_CONTAINER), false)
	@golangci-lint run -c .golangci.toml
else
	@DOCKER_IMAGE=golangci/golangci-lint:v1.16.0 TARGET=linter $(MAKE) docker-exec
endif

linter-vendor:
ifeq ($(EXEC_CONTAINER), false)
	@go mod tidy
	@go mod vendor
	@git diff --exit-code
else
	@TARGET=linter-vendor make docker-exec
endif

docker-exec:
	@docker run \
		-t \
		--rm \
		-e EXEC_CONTAINER=false \
		-e GO111MODULES=$(GO111MODULES) \
		-e TAGS=$(TAGS) \
		-e "TERM=xterm-256color" \
		-v $(PWD):/opt/form3/payment \
		-w /opt/form3/payment \
		$(DOCKER_IMAGE) \
		make $(TARGET)