# <img src="misc/assets/images/icon.png" width="5%"/> Form3 Payment [![pipeline status](https://gitlab.com/diego.bernardes/payment/badges/master/pipeline.svg)](https://gitlab.com/diego.bernardes/payment/commits/master) [![coverage report](https://gitlab.com/diego.bernardes/payment/badges/master/coverage.svg)](https://gitlab.com/diego.bernardes/payment/commits/master)
This is the solution for Form3 coding exercise.

## Running the application
Postgres is the database used by this project. The snippet below can be used to quickly run an instance:
```shell
docker run --rm -p 5432:5432 -e POSTGRES_DB=form3 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_HOST=localhost postgres:11.2
```

Don't forget to create the database `form3`, the Docker image already does this for you. After that, the database should be bootstrapped using this file: `misc/sql/bootstrap.sql`.

Now the project is ready to run:
```shell
cd cmd/server
go run main.go start
```

## Running the tests
There are several tasks at the make file to check the source code:
```shell
make test
make linter
make linter-vendor
```

Also, there is one that executes all quality steps: `make pre-pr`.

### Integration tests
To execute the integration tests, some environment variables should be set:
```shell
export POSTGRES_USER=postgres
export POSTGRES_PASSWORD=postgres
export POSTGRES_HOST=localhost
export POSTGRES_DB=form3
export TAGS=integration
export EXEC_CONTAINER=false

make test
```

## Documentation
The application has an OpenAPI specification at `misc/openapi/openapi.yml`. The spec is also available as pdf `misc/openapi/openapi.pdf`.
