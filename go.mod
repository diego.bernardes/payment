module form3/payment

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.3.0+incompatible
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.0.0 // indirect
	github.com/oklog/ulid v1.3.1
	github.com/pkg/errors v0.8.1
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/shopspring/decimal v0.0.0-20180709203117-cd690d0c9e24 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.2
	github.com/stretchr/testify v1.3.0
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.9.1
	golang.org/x/sync v0.0.0-20190412183630-56d357773e84
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
