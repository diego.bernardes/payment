CREATE TABLE "payments" (
  id      SERIAL,
  base_id VARCHAR NOT NULL UNIQUE,
  raw     VARCHAR NOT NULL
);