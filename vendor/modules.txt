# github.com/davecgh/go-spew v1.1.1
github.com/davecgh/go-spew/spew
# github.com/fsnotify/fsnotify v1.4.7
github.com/fsnotify/fsnotify
# github.com/go-chi/chi v4.0.2+incompatible
github.com/go-chi/chi
# github.com/hashicorp/hcl v1.0.0
github.com/hashicorp/hcl
github.com/hashicorp/hcl/hcl/printer
github.com/hashicorp/hcl/hcl/ast
github.com/hashicorp/hcl/hcl/parser
github.com/hashicorp/hcl/hcl/token
github.com/hashicorp/hcl/json/parser
github.com/hashicorp/hcl/hcl/scanner
github.com/hashicorp/hcl/hcl/strconv
github.com/hashicorp/hcl/json/scanner
github.com/hashicorp/hcl/json/token
# github.com/inconshreveable/mousetrap v1.0.0
github.com/inconshreveable/mousetrap
# github.com/jackc/pgx v3.3.0+incompatible
github.com/jackc/pgx
github.com/jackc/pgx/internal/sanitize
github.com/jackc/pgx/pgio
github.com/jackc/pgx/pgproto3
github.com/jackc/pgx/pgtype
github.com/jackc/pgx/chunkreader
# github.com/magiconair/properties v1.8.0
github.com/magiconair/properties
# github.com/mitchellh/mapstructure v1.1.2
github.com/mitchellh/mapstructure
# github.com/oklog/ulid v1.3.1
github.com/oklog/ulid
# github.com/pelletier/go-toml v1.2.0
github.com/pelletier/go-toml
# github.com/pkg/errors v0.8.1
github.com/pkg/errors
# github.com/pmezard/go-difflib v1.0.0
github.com/pmezard/go-difflib/difflib
# github.com/spf13/afero v1.1.2
github.com/spf13/afero
github.com/spf13/afero/mem
# github.com/spf13/cast v1.3.0
github.com/spf13/cast
# github.com/spf13/cobra v0.0.3
github.com/spf13/cobra
# github.com/spf13/jwalterweatherman v1.0.0
github.com/spf13/jwalterweatherman
# github.com/spf13/pflag v1.0.3
github.com/spf13/pflag
# github.com/spf13/viper v1.3.2
github.com/spf13/viper
# github.com/stretchr/testify v1.3.0
github.com/stretchr/testify/require
github.com/stretchr/testify/assert
# go.uber.org/atomic v1.3.2
go.uber.org/atomic
# go.uber.org/multierr v1.1.0
go.uber.org/multierr
# go.uber.org/zap v1.9.1
go.uber.org/zap
go.uber.org/zap/internal/bufferpool
go.uber.org/zap/zapcore
go.uber.org/zap/buffer
go.uber.org/zap/internal/color
go.uber.org/zap/internal/exit
# golang.org/x/sync v0.0.0-20190412183630-56d357773e84
golang.org/x/sync/errgroup
# golang.org/x/sys v0.0.0-20181205085412-a5c9d58dba9a
golang.org/x/sys/unix
# golang.org/x/text v0.3.0
golang.org/x/text/transform
golang.org/x/text/unicode/norm
# gopkg.in/yaml.v2 v2.2.2
gopkg.in/yaml.v2
